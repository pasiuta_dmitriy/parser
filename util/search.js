function transformJsonLinks(data) {
    const links = [];

    for (let i in data.organic_results) {
        const link = data.organic_results[i].link;
        links.push(link);
    }

    return links;
}

module.exports = {
    transformJsonLinks
}

