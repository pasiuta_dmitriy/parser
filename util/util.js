const timeout = millis => new Promise(resolve => setTimeout(resolve, millis))

module.exports = {
    timeout
}
