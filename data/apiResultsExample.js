const apiResultExample = {
    "organic_results": [
        {
            "title": "All The Little Things (Undercatt Remix) [Eleatics Records]",
            "link": "https://soundcloud.com/progressiveastronaut/premiere-giorgia-angiuli-all-the-little-things-undercatt-remix-eleatics-records"
        },
        {
            "title": "Giorgia Angiuli - All The Little Things (Undercatt Remix)",
            "link": "https://techmusic.ru/load/melodic_house_techno/giorgia_angiuli_all_the_little_things_undercatt_remix/5-1-0-65880"
        },
        {
            "title": "Giorgia Angiuli - All The Little Things (Undercatt Remix)",
            "link": "http://clubtone.do.am/music/club_music/giorgia_angiuli_all_the_little_things_undercatt_remix/2-1-0-607581"
        },
        {
            "title": "All The Little Things (Undercatt Remix) | Play on Anghami",
            "link": "https://play.anghami.com/song/1025302795"
        },
        {
            "title": "All the Little Things (Undercatt Remix) [Mixed] - Giorgia Angiuli",
            "link": "https://www.shazam.com/track/575877236/all-the-little-things-undercatt-remix-mixed"
        }
    ]
};

module.exports = {
    apiResultExample
}
