const { transformJsonLinks } = require("./util/search");
const { apiResultExample } = require("./data/apiResultsExample");
const { launch } = require("./parser/parser");

// q: 'song title1, song title2`
// 1. split -> ['t1','t2']
// 2. getSongs()
const links = transformJsonLinks(apiResultExample);

const complainData = {
    email: 'testpiratemusicsites@gmail.com',
    labelEmail: 'eleaticsrecords@gmail.com',
    password: 'testpirate2000',
    name: 'Viacheslav',
    surname: 'Sen',
    nameSurname: 'Viacheslav Sen',
    companyName: 'LLC ELSP Company Eleatics Records',
    copyrightedText: 'Being the owner of the exclusive copyrights and having a good belief that the use of the ' +
        'copyrighted work is not authorized by the copyright owner, here with requesting the removal of the requested ' +
        'material from your site. Here with confirming that the information in this notice is ' +
        'accurate and the company is the copyright owner',
    beatportLink: 'https://www.beatport.com/track/respect-me/15483522',
    pirateLink: 'http://clubtone.do.am/music/club_music/8kays_lazarusman_respect_me_rodriguez_jr_remix/2-1-0-609618',
    data: '02/08/2022'
};

(async function start() {
    // getSongs([]) -> links

    for await (const link of links) {
        const complainDataStep = { ...complainData }; // copy
        complainDataStep.pirateLink = link;
        //complainDataStep.beatportLink = link;

        console.log('--------------')
        console.log(complainDataStep)
        console.log('--------------')
        const result = await launch(complainDataStep);
        console.log(`${link} has been executed with success ${result}`);
    }
})();
