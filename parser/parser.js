const puppeteer = require("puppeteer");
const request = require("request-promise-native");
const { default: poll } = require("promise-poller");

const { timeout } = require("../util/util");
const { chromeOptions, config } = require("../config/config");

async function launch(complainData) {
    return new Promise (async (resolve, reject) => {
        // headless: false
        // to see the result in the browser
        const browser = await puppeteer.launch(chromeOptions);
        const page = await browser.newPage();
        await page.setViewport({width: 1280, height: 720});
        // let's do some navigation
        await page.goto("https://support.google.com/legal/troubleshooter/1114905?pli=1");

        // access history and evaluate last url of page
        const session = await page.target().createCDPSession();
        const history = await session.send("Page.getNavigationHistory");
        const last = history.entries[history.entries.length - 2];

        await page.click('#gb a.gb_1.gb_1.gb_2d')
        await page.waitForSelector('input[type="email"]')
        await page.type('input[type="email"]', complainData.email);
        await Promise.all([
            page.waitForNavigation(),
            await page.keyboard.press('Enter')
        ]);
        await page.waitForSelector('input[type="password"]', {visible: true});
        await page.type('input[type="password"]', complainData.password);
        await Promise.all([
            page.waitForNavigation(),
            await page.keyboard.press('Enter')
        ]);

        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(3) > div.question > label:nth-child(3) > span')
            await page.click('div:nth-child(3) > div.question > label:nth-child(3) > span');

        }, 1000)
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(3) > div:nth-child(2) > div.question > label:nth-child(3) > span')
            await page.click('div:nth-child(3) > div:nth-child(2) > div.question > label:nth-child(3) > span');
        }, 2000)
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div.question > label:nth-child(7) > span');
            await page.click('div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div.question > label:nth-child(7) > span');
        }, 3000)
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(3) > div:nth-child(2) > div:nth-child(2) > div:nth-child(8) > div.question > label:nth-child(3) > span');
            await page.click('div:nth-child(3) > div:nth-child(2) > div:nth-child(2) > div:nth-child(8) > div.question > label:nth-child(3) > span');
        }, 4000)
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(3) > div:nth-child(2) > div:nth-child(2) > div:nth-child(8) > div:nth-child(3) > div.question > label:nth-child(3) > span');
            await page.click('div:nth-child(3) > div:nth-child(2) > div:nth-child(2) > div:nth-child(8) > div:nth-child(3) > div.question > label:nth-child(3) > span');

        }, 5000)
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(3) > div:nth-child(2) > div:nth-child(2) > div:nth-child(8) > div:nth-child(3) > div:nth-child(2) > div.question > label:nth-child(4) > span');
            await page.click('div:nth-child(3) > div:nth-child(2) > div:nth-child(2) > div:nth-child(8) > div:nth-child(3) > div:nth-child(2) > div.question > label:nth-child(4) > span');
        }, 6000)
        setTimeout(async () => {
            await page.waitForSelector('.hcfe .btn.btn-raised');
            await page.click('.hcfe .btn.btn-raised')
        }, 9000)

        await page.waitForSelector('input[name="first-name"]')
        await page.type('input[name="first-name"]', complainData.name);
        await page.waitForSelector('input[name="last-name"]')
        await page.type('input[name="last-name"]', complainData.surname);
        await page.waitForSelector('input[name="company-name"]')
        await page.type('input[name="company-name"]', complainData.companyName);

        setTimeout(async () => {
            await page.waitForSelector('#add-copyright-holder-link');
            await page.click('#add-copyright-holder-link');
        }, 6000)
        setTimeout(async () => {
            await page.waitForSelector('input[id="new-copyright-holder"]');
            await page.type('input[id="new-copyright-holder"]', complainData.nameSurname);
        }, 7200);
        setTimeout(async () => {
            await page.click('.modal-dialog-buttons .goog-buttonset-action')
        }, 8000);
        setTimeout(async () => {
            await page.click('#confirm-self-cr-holder')
        }, 9000);
        setTimeout(async () => {
            await page.waitForSelector('input[name="email"]')
            await page.type('input[name="email"]', complainData.labelEmail);
        }, 10500);
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(20) > select')
            await page.click('div:nth-child(20) > select');
        }, 11000);
        setTimeout(async () => {
            await page.select('select[name="country-code"]', 'UA');
        }, 12000);
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(2) > div:nth-child(4) > textarea')
            await page.type('div:nth-child(2) > div:nth-child(4) > textarea', complainData.copyrightedText);
        }, 16000);
        setTimeout(async () => {
            await page.waitForSelector('textarea[name="cr-work-urls0"]')
            await page.type('textarea[name="cr-work-urls0"]', complainData.beatportLink);
        }, 18000);
        setTimeout(async () => {
            await page.waitForSelector('textarea[name="infringing-urls0"]')
            await page.type('textarea[name="infringing-urls0"]', complainData.pirateLink);
        }, 20000);
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(28) > div > label > input[type=checkbox]')
            await page.click('div:nth-child(28) > div > label > input[type=checkbox]');
        }, 21000);
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(29) > div > label > input[type=checkbox]')
            await page.click('div:nth-child(29) > div > label > input[type=checkbox]');
        }, 22000);
        setTimeout(async () => {
            await page.waitForSelector('div:nth-child(30) > div > label > input[type=checkbox]')
            await page.click('div:nth-child(30) > div > label > input[type=checkbox]');
        }, 23000);
        setTimeout(async () => {
            await page.waitForSelector('input[name="signature-date"]')
            await page.type('input[name="signature-date"]', complainData.data);
        }, 24000);
        setTimeout(async () => {
            await page.waitForSelector('input[name="signature"]')
            await page.type('input[name="signature"]', complainData.nameSurname);
        }, 25000);
        setTimeout(async () => {
            // const requestId = await initiateCaptchaRequest(config.apikey);
            // const response = await pollForRequestResults(config.apikey, requestId);
            // await page.evaluate(`document.getElementById("g-recaptcha-response").innerHTML="${response}"`)
            // await page.click('#dmca > input[type=submit]:nth-child(45)');


            ///////// <---------
            resolve(true)
            /////////
        }, 25000);
    });
}

async function initiateCaptchaRequest(apiKey) {
    console.log(apiKey);
    const formData = {
        method: 'userrecaptcha',
        googlekey: config.sitekey,
        key: apiKey,
        pageurl: config.pageurl,
        json: 1
    };
    console.log(`Submitting solution request to 2captcha for ${config.pageurl}`);
    const response = await request.post(config.apiSubmitUrl, {form: formData});
    return JSON.parse(response).request;
}

async function pollForRequestResults(key, id, retries = 70, interval = 2000, delay = 3000) {
    console.log(`Waiting for ${delay} milliseconds`)
    await timeout(delay);
    return poll({
        taskFn: requestCaptchaResults(key, id),
        interval,
        retries
    });
}

function requestCaptchaResults(apiKey, requestId) {
    const url = `${config.apiRetrieveUrl}?key=${apiKey}&action=get&Id=${requestId}&json=1`;
    return async function () {
        return new Promise(async function (resolve, reject) {
            console.log(`Polling for responce...`)
            const rawResponse = await request.get(url);
            const resp = JSON.parse(rawResponse);
            console.log(resp)
            if (resp.status === 0) return reject(resp.request);
            console.log('Response received.')
            resolve(resp.request);
        });
    }
}

module.exports = {
    launch
}
